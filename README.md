# Team B | Continuous Delivery Workshop 2.0

[![pipeline status](https://gitlab.com/drx-tw/cdws-2.0-team-b/badges/master/pipeline.svg)](https://gitlab.com/drx-tw/cdws-2.0-team-b/commits/master)

This is a **Continuous Delivery Lab**, and it's fork from [@chusiang/continuous-delivery-workshop](https://gitlab.com/chusiang/continuous-delivery-workshop).

## Members

1. [Chu-Siang Lai]: https://github.com/chusiang/
